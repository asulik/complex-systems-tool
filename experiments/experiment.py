import argparse


class Experiment:
    def __init__(self):
        pass

    def __call__(self):
        raise NameError("Callable not implemented")

    @classmethod
    def name(cls) -> str:
        return cls.__name__

    def save_outputs(self):
        print("Ooops, looks like there is nothing to be saved")
