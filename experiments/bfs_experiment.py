from pathlib import Path
from datetime import datetime

import math
import numpy as np
import pandas as pd

from networktools.network import Network
from networktools.traversing import breadth_first_search
from .experiment import Experiment
from .decorator import register_experiment


@register_experiment
class BfsResearch(Experiment):
    def __init__(self):
        super().__init__()
        self.output_dict = {'net_type': [], 'mean_k': [], 'number_of_agents': [], 'mean_distance': []}
        self.bfs = {'name': [], 'data': []}

    def __call__(self):

        n_values = np.array([100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000])
        k_values = [4, 10]

        for net_type in ['ER', 'BA']:
            for k in k_values:
                for n in n_values:
                    now = datetime.now()
                    print(f'{now.strftime("%H:%M:%S")}\t'
                          f'Calculating {net_type} distances, k={k}, N={n}')

                    if net_type == 'ER':
                        er_p = k/n
                        net = Network('classic_er', agents_num=n, prob=er_p)
                    else:
                        ba_m = math.ceil(k/2)
                        net = Network('ba', m=ba_m, m0=ba_m, agents_num=n)

                    bfs = breadth_first_search(net)
                    md = self._calculate_mean_distance(bfs)

                    # save calculations
                    self.output_dict['net_type'].append(net_type)
                    self.output_dict['mean_k'].append(k)
                    self.output_dict['number_of_agents'].append(n)
                    self.output_dict['mean_distance'].append(md)

                    # save flattened distances vector
                    self.bfs['data'].append(bfs[np.mask_indices(bfs.shape[0], np.triu, 1)])
                    self.bfs['name'].append(f"{net_type}k{k}n{n}")

    def save_outputs(self):
        outputs = pd.DataFrame(self.output_dict)

        stamp = str(datetime.now()).replace(' ', '_').replace('.', '_').replace(":", "-")
        output_path = Path('./outputs/') / self.__class__.__name__ / stamp
        output_path.mkdir(parents=True)

        outputs.to_csv(output_path / 'bfs.csv', index=False)

        for i in range(len(self.bfs['name'])):
            np.savetxt(output_path / (self.bfs['name'][i].lower()+"txt"), self.bfs['data'][i], fmt="%d")

        print("Saved to: " + str(output_path))
        print("Files net_name_xk_yN contains raveled upper triangle of distances matrix")

    @staticmethod
    def _calculate_mean_distance(distances_matrix: np.ndarray):
        return np.mean(distances_matrix[np.mask_indices(distances_matrix.shape[0],
                                                        np.triu, 1)])
