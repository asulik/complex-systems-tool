from pathlib import Path
from datetime import datetime

import numpy as np

from networktools.network import Network
from .experiment import Experiment
from .decorator import register_experiment


@register_experiment
class NetworkParameters(Experiment):
    def __init__(self):
        super().__init__()
        self.net = Network('mc_er', agents_num=20, prob=0.7)

    def __call__(self):
        print("That's an example experiment. Look at this small ER graph statistics for p=0.7")
        print('Degrees:')
        print(self.net.k)


    def save_outputs(self):
        stamp = str(datetime.now()).replace(' ', '_').replace('.', '_')

        output_path = Path('./outputs/') / self.__class__.__name__ / stamp
        output_path.mkdir(parents=True)

        print("Saved to: " + str(output_path))
        output = open(output_path / 'output.txt', 'w')
        output.write('Degrees:')
        output.write(str(self.net.k))
