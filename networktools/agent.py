# agent.py>

import numpy as np


class Agent:
    def __init__(self, id: int):
        self.id = id
        self.neighbours = np.array([])

    def __getitem__(self, key):
        return self.neighbours[key]

    def __len__(self):
        if len(self.neighbours):
            return len(self.neighbours)
        else:
            return 0

    def __iter__(self):
        return _AgentIterator(self)

    def add_neighbour(self, nid):
        self.neighbours = np.append(self.neighbours, nid).astype(int)

    def print(self):
        print(f'Agent {self.id}')
        print(f'Neighbours: {self.neighbours}')
        print(f'K: {len(self)}')
        print()


class _AgentIterator:
    def __init__(self, agent: Agent):
        self._agent = agent
        self._index = 0

    def __next__(self):
        if self._index < len(self._agent):
            to_return = self._agent[self._index]
            self._index += 1
            return to_return

        raise StopIteration
