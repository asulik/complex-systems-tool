# Network Science Experiments Framework
Author: Adam Sulik

Welcome to the Network Science Experiments Framework, a versatile tool for conducting experiments in the field of network science. This repository provides a structured environment to easily design, run, and analyze experiments related to network analysis.

1. Explore the available experiments by running:
```bash
python3 main.py --help
```

2. To get detailed information about a specific experiment, use:
```bash 
python3 main.py experiment_name --help
```
## Running an experiment
 To run an experiment, simply execute the main.py script with the desired experiment name. For example:
```bash 
python3 main.py sample_experiment --arguments...
```

## Adding an experiment
To contribute a new experiment, adhere to the following guidelines:

1. Create a new Python file in the experiments/ directory.
2. Define a class for your experiment and implement the __call__ method, which represents the main functionality of the experiment.
3. Document the experiment's usage by adding a help message to the __init__ method of your class.

Here's a simple example of how to add a new experiment:
```python
# experiments/sample_experiment.py
from .experiment import Experiment
from .decorator import register_experiment

@register_experiment
class SampleExperiment(Experiment):
    def __init__(self):
        self.name = "sample_experiment"
        self.description = "A sample experiment to demonstrate the framework."

    def __call__(self, *args, **kwargs):
        # Your experiment code goes here
        print("Running the sample experiment...")
```
Don't forget about inheriting after `Experiment` and registering the experiment by the above used decorator.
