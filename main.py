import argparse
import sys

from experiments import EXPERIMENTS


def main(args):
    experiment()
    print('Saving outputs...')
    experiment.save_outputs()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("experiment_name",
                        metavar='experiment_name',
                        type=str,
                        choices=EXPERIMENTS.keys(),
                        help="Choose an experiment to run. Available: "+str(list(EXPERIMENTS.keys())),
                        )

    args = parser.parse_args()

    experiment = EXPERIMENTS[args.experiment_name]()
    sys.exit(main(args) or 0)
